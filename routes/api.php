<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get( '/reservations/{date}', 'ReservationController@get_from_date' );
Route::get( '/upcoming-reservations', 'ReservationController@upcoming_reservations' );
Route::get( '/past-reservations', 'ReservationController@past_reservations' );

