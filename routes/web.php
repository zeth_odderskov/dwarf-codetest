<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get( '/', 'GenericController@home' )->name( 'home' );
Route::get( '/reserve-table', 'GenericController@reserve_table' )->name( 'reserve-table' );
Route::get( '/contact', 'GenericController@contact' )->name( 'contact' );

Route::resource('reservations', 'ReservationController');

Route::group(['prefix' => '/',  'middleware' => ['auth']], function(){
	Route::get( '/admin/dashboard', 'BackendController@dashboard' )->name( 'dashboard' );

	Route::get( '/admin/restaurant-settings', 'BackendController@restaurant_settings' )->name( 'restaurant_settings' );
	Route::post( '/admin/save-restaurant-settings', 'BackendController@save_restaurant_settings' )->name( 'save_restaurant_settings' );
});

Auth::routes();


