<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
	protected $table = 'reservations';

	protected $fillable = [ 'id', 'created_at', 'updated_at', 'customer_id', 'reservation_start', 'amount_of_seats', 'drinks_of_choice', 'dishes_of_choice' ];

	public function customer(){
		return $this->belongsTo( 'App\Customer', 'customer_id' );
	}
}
