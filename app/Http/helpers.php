<?php
/**
 * Add flash message to the session
 *
 * Types:
 *  - primary
 *  - secondary
 *  - success
 *  - danger
 *  - warning
 *  - info
 *  - light
 *  - dark
 */
function add_flash_message( $title = '', $message = '', $type = ''){

	session()->flash( 'any_notifications', true );

	if(
		empty( session( 'notification_collection' ) )
	){
		$notification_collection = new \Illuminate\Support\Collection();
	} else {
		$notification_collection = \Session::get( 'notification_collection' );
	}

	$notification_collection->push( [
		'notification_title' => $title,
		'notification_message' => $message,
		'notification_type' => $type,
	]);
	session()->flash( 'notification_collection', $notification_collection );

}


?>

