<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BackendController extends Controller
{

	public function dashboard(){
		return view( 'admin.dashboard' );
	}

	public function restaurant_settings(){

		$options_number_of_tables = DB::table('options')->where('key',  'number_of_tables' )->first();
		if( !empty( $options_number_of_tables ) ){
			$number_of_tables = $options_number_of_tables->value;
		} else {
			$number_of_tables = 0;
		}

		return view( 'admin.restaurant-settings', [
			'number_of_tables' => $number_of_tables
		] );
	}

	public function save_restaurant_settings( Request $request ){

		$number_of_tables = $request->number_of_tables;

		if(
			isset( $number_of_tables ) &&
			is_integer( (int) $number_of_tables ) &&
			$number_of_tables < 30
		){

			$options = DB::table('options')->where('key',  'number_of_tables' )->first();
			if( isset( $options ) ){

				DB::table('options')->where( 'id', $options->id )->update(
					[ 'value' => $number_of_tables ] );
				add_flash_message( 'Success!', 'Number of tables were updated.', 'success' );
			} else {

				DB::table('options')->insert(
					[
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now(),
						'key' => 'number_of_tables',
						'value' => $number_of_tables
					]
				);
				add_flash_message( 'Success!', 'Number of tables were saved.', 'success' );
			}

			if( $number_of_tables == 0 ) {
				add_flash_message( 'Ehm... Hmm!', "You've set the amount of tables to 0, which basically deactivates the restaurant. I hope you know what you're doing!", 'dark' );
			}
		} else {
			add_flash_message( 'Ehm... Hmm!', 'Something went wrong. Please try again', 'danger' );
		}

//		$restaurant_options = DB::table('options')->get();
		return redirect( Route( 'restaurant_settings' ) );
	}


	public function reservation_overview(){
		return view( 'admin.reservation-overview' );
	}

//Route::get( '/admin/restaurant-settings', 'BackendController@restaurant_settings' )->name( 'restaurant_settings' );
//Route::get( '/admin/reservation-overview', 'BackendController@reservation_overview' )->name( 'reservation_overview' );

}
