<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenericController extends Controller
{
	public function home(){
		return view( 'pages.home' );
	}

	public function reserve_table(){

		$options_number_of_tables = DB::table('options')->where('key',  'number_of_tables' )->first();
		if( !empty( $options_number_of_tables ) ){
			$number_of_tables = $options_number_of_tables->value;
		} else {
			$number_of_tables = 0;
		}

		return view( 'pages.reserve-table', [
			'number_of_tables' => $number_of_tables
		] );
	}

	public function contact(){
		return view( 'pages.contact' );
	}
}
