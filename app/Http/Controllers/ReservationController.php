<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Reservation;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReservationController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(){
		//
	}





	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(){
		//
	}





	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ){
		//

		$customer = Customer::firstOrCreate(
			['email' => $request->email],
			[
				'name' => $request->name,
				'phone' => $request->phone
			]
		);

		$reservation = new Reservation();
		$reservation->customer_id = $customer->id;
		$reservation->reservation_start = Carbon::createFromFormat( 'Y-m-d H', $request->date . ' ' . $request->time  );
		$reservation->amount_of_seats = $request->seats;
		$reservation->dishes_of_choice = $request->dish;
		$reservation->drinks_of_choice = $request->drinks;
		$reservation->save();

		return view( 'pages.reservation-confirmation', [
			'customer' => $customer,
			'reservation' => $reservation
		] );
	}





	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ){
		//
	}





	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function get_from_date($date){

		// Validating the date format
		preg_match( '/^\d{4,4}-\d{2,2}-\d{2,2}$/', $date, $date_has_valid_format);

		if( empty( $date_has_valid_format ) ){
			return response()->json([
					'error' => 'Date has a wrong format. It should be YYYY-MM-DD'
				]);
		} else {
			$from_date = \Carbon\Carbon::createFromFormat( 'Y-m-d H:i', $date . ' 00:00' );
			$to_date = \Carbon\Carbon::createFromFormat( 'Y-m-d H:i', $date . ' 23:59' );

			$reservations = Reservation::where([
				[ 'reservation_start', '>', $from_date->format( 'Y-m-d H:i' )],
				[ 'reservation_start', '<', $to_date->format( 'Y-m-d H:i' ) ]
			])->get();

			// Todo - Purge response, so customer-ID and dishes and other 'sensitive' information isn't available to anyone.

			return response()->json([
				'reservations' => $reservations
			]);
		}
	}






	/**
	 * Display the specified resource.
	 *
	 * Todo - Ensure that the user is logged in, before sending response.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function upcoming_reservations(){

		$date = \Carbon\Carbon::now();

		$reservations = Reservation::where([
			[ 'reservation_start', '>', $date->format( 'Y-m-d H:i' )],
		])->orderBy( 'reservation_start', 'ASC' )->with('customer')->get();
//		$reservations->orderBy( 'reservation_start', 'DESC' );
//		$reservations->sortBy( 'reservation_start' );
//		dd( $reservations );


		return response()->json([
			'reservations' => $reservations
		]);
	}






	/**
	 * Display the specified resource.
	 *
	 * Todo - Purge response, so customer-ID and dishes and other 'sensitive' information isn't available to anyone.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function past_reservations(){

		$date = \Carbon\Carbon::now();

		$reservations = Reservation::where([
			[ 'reservation_start', '<', $date->format( 'Y-m-d H:i' )],
		])->orderBy( 'reservation_start', 'DESC' )->with('customer')->get();

		return response()->json([
			'reservations' => $reservations
		]);
	}







	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ){
		//
	}





	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ){
		//
	}





	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ){
		//
	}
}
