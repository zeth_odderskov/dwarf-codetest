//require('./bootstrap');



/**
 * jQuery
 */
window.$ = window.jQuery = require('jquery');


/**
 * Bootstrap
 */
require( 'bootstrap/dist/js/bootstrap.bundle.min.js' );


/**
 * Polyfill for Object-fit
 *
 * IE11-fix for `object-fit: cover;`
 * Source: https://github.com/bfred-it/object-fit-images
 */
import objectFitImages from 'object-fit-images';

jQuery( document ).ready( function () {
	objectFitImages();
});






/**
 * Vue
 */
// import Vue from 'vue';
window.Vue = require('vue');

// Moment
Vue.use(require('vue-moment'));

// Axios
import axios from 'axios';
import VueAxios from 'vue-axios';
Vue.use(VueAxios, axios);


/**
 * Components
 */
// import SundownComponent from './components/SundownComponent.vue';
import SundownReserveTable from './components/SundownReserveTable.vue';
import SundownPastReservations from './components/SundownPastReservations.vue';
import SundownUpcomingReservations from './components/SundownUpcomingReservations.vue';

const app = new Vue({
	el: '#app',
	components: {
		SundownReserveTable,
		SundownUpcomingReservations,
		SundownPastReservations
	},
	data: {
	},
	watch: {
	},
	methods: {
	},
	computed: {
	},
	mounted() {
	},
	beforeCreate(){


	},
	created() {
	},
	destroyed() {
	}
});

