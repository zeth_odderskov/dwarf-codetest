<nav class="navbar navbar-expand-sm">
	<a class="logo" href="/">
		<img src="{{ url( '/img/sundown-boulevard-logo.jpg' ) }}" alt="Sundown Boulevard" class="">
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav ml-auto">

				<li class="nav-item">
					<a class="nav-link" title="Sundown Boulevard" href="{{ Route( 'home' ) }}">Home</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" title="Reserve a table" href="{{ Route( 'reserve-table' ) }}">Reserve a table</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" title="Contact Sundown Boulevard" href="{{ Route( 'contact') }}">Contact</a>
				</li>

			@auth

				<li class="nav-item nav-item__dashboard">
					<a class="nav-link" title="Dashboard" href="{{ Route( 'dashboard') }}">Dashboard</a>
				</li>

				<li class="nav-item dropdown">
					<a id="profileDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false" v-pre>
						{{ Auth::user()->name }} <span class="caret"></span></a>

					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="profileDropdown">

						<a class="dropdown-item" href="{{ route('logout') }}"
							onclick="event.preventDefault();
																											 document.getElementById('logout-form').submit();">
							{{ __('Logout') }}
						</a>

						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					</div>
				</li>

			@endauth

		</ul>
	</div>
</nav>

