<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

@if( empty( $title_tag ) )
	<title>Sundown Boulevard</title>
@else
	<title>{{ $title_tag }}</title>
@endif

@if( empty( $meta_desc ) )
	<meta name="description" content="Sundown Boulevard is your goto place for drinks and steaks!">
@else
	<meta name="description" content="{{ $meta_desc }}">
@endif

<meta name="author" content="Zeth">
<meta name="distribution" content="Global"/>
<meta name="rating" content="Safe For Kids"/>
<meta name="copyright" content="Zeth"/>

<!-- Styles -->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" type="image/png" href="{{ url( '/img/sundown-boulevard-favicon.png' ) }}">
