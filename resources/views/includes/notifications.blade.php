@if( \Session::has( 'any_notifications' ) )
	<div class="container flash-message">
		<div class="row">
			<div class="col-12">

				@foreach ( \Session('notification_collection') as $notification)
					<div class="alert alert-{{ $notification['notification_type'] }}" role="alert">
						<strong>{{ $notification['notification_title'] }}</strong> - {{ $notification['notification_message'] }}
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					</div>
				@endforeach
			</div>
		</div>
	</div>
	<!-- /.container -->

	{!! \Session::forget( 'notification_collection' ) !!}
	{!! \Session::forget( 'any_notification' ) !!}
@endif


