@auth
	<div class="container-fluid admin-subheader__outer-container">
		<div class="row">
			<div class="col-12">
				<ul>
					<li>
						<a href="{{ Route( 'restaurant_settings' ) }}">{{ __( 'Restaurant settings') }}</a>
					</li>
				</ul>
			</div>
			<!-- /.col-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container-fluid -->
@endauth

