@extends('layouts.default',
	[
	'body_class' => 'body__home',
	'html_class' => '',
	'title_tag' => '',
	'meta_desc' => ''
	]
)



@section('css')
@stop



@section('js_head')
@stop



@section('content')

		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<h1 class="mb-3">Thank you for your reservation</h1>

					<p><strong>Details</strong></p>

					<ul>
						<li><strong>Name: </strong> {{ $customer->name }}</li>
						<li><strong>Email: </strong> {{ $customer->email }}</li>
						<li><strong>Phone: </strong> {{ $customer->phone }}</li>
						<li><strong>Reservation date and time: </strong> {{ $reservation->reservation_start->format( 'Y-m-d H:i' ) }}</li>
						<li><strong>Seats: </strong> {{ $reservation->amount_of_seats }}</li>
						<li><strong>Dish: </strong> {{ $reservation->dishes_of_choice }}</li>
						<li><strong>Drinks: </strong> {{ $reservation->drinks_of_choice }}</li>
					</ul>


				</div>
				<!-- /.col-md-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->

@stop


