@extends('layouts.default',
	[
	'body_class' => 'body__home',
	'html_class' => '',
	'title_tag' => '',
	'meta_desc' => ''
	]
)



@section('css')
@stop



@section('js_head')
@stop



@section('content')

		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<h1>Sundown Boulevard - Home</h1>

					<img src="{{ url( '/img/sundown-boulevard-3.jpg' ) }}" alt="Sundown Boulevard">

				</div>
				<!-- /.col-md-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->

@stop


