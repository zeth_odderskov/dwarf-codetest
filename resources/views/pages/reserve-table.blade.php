@extends('layouts.default',
	[
	'body_class' => 'body__reserve-table',
	'html_class' => '',
	'title_tag' => '',
	'meta_desc' => ''
	]
)



@section('css')
@stop



@section('js_head')
@stop



@section('content')

		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<h1>Reserve table</h1>

					@if( $number_of_tables > 0 )
						<sundown-reserve-table
							:number-of-tables="<?php echo $number_of_tables; ?>"
						>
							{{ csrf_field() }}
						</sundown-reserve-table>
					@else
						<p>It looks like that the restaurant hasn't set their number of tables in the dashboard.</p>
					@endif

				</div>
				<!-- /.col-md-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->

@stop


