@extends('layouts.default',
	[
	'body_class' => 'body__contact',
	'html_class' => '',
	'title_tag' => '',
	'meta_desc' => ''
	]
)



@section('css')
@stop



@section('js_head')
@stop



@section('content')

		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<h1>Contact</h1>
					<p>Phone: <a href="tel:12345678">123 45 678</a></p>
					<p>Email: <a href="tel:hi@sundown-boulevard.com">hi@sundown-boulevard.com</a></p>

				</div>
				<!-- /.col-md-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->

@stop


