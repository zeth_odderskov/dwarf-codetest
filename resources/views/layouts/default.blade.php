<!doctype html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml" class="{{ $html_class }}">
<head>
	@include('includes.head')
	@yield('css')
	@yield('js_head')
</head>
<body class="{{ $body_class }}">
<header class="header">
	@include('includes.header')
</header>
<!-- /.header -->

<main id="app">
	@include('includes.notifications')
	@yield('content')
	<div class="main__background-image">
	</div>
	<!-- /.main__background-image-container -->
</main>
<!-- /#main -->

<footer class="footer">
	@include('includes.footer')
</footer>
<!-- /.footer -->

@include('includes.footer-scripts')
</body>
</html>
