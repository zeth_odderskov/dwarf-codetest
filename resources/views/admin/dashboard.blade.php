@extends('layouts.backend',
	[
	'body_class' => 'body__dashboard',
	'html_class' => '',
	'title_tag' => '',
	'meta_desc' => ''
	]
)



@section('css')
@stop



@section('js_head')
@stop



@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h1>Dashboard</h1>

				<hr class="my-5">

				<h4 class="text-uppercase font-weight-bold">Upcoming reservations</h4>

				<sundown-upcoming-reservations>
				</sundown-upcoming-reservations>

				<hr class="my-5">

				<h4 class="text-uppercase font-weight-bold">Past reservations</h4>

				<sundown-past-reservations>
				</sundown-past-reservations>


			</div>
			<!-- /.col-md-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->

@stop

