@extends('layouts.backend',
	[
	'body_class' => 'body__dashboard',
	'html_class' => '',
	'title_tag' => '',
	'meta_desc' => ''
	]
)



@section('css')
@stop



@section('js_head')
@stop



@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-2">

				<h1 class="mb-5">Restaurant settings</h1>

				<form method="post" action="{{ Route( 'save_restaurant_settings' ) }}">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="number_of_tables">Number of tables in the restaurant (2 seats per table)</label>
						<input class="form-control" type="number" id="number_of_tables" name="number_of_tables" value="{{ $number_of_tables }}">
					</div>
					<button type="submit" class="btn btn-primary ml-auto d-block">Submit</button>
				</form>



			</div>
			<!-- /.col-md-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->

@stop

