<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'customers', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->string( 'name' );
			$table->string( 'email' )->unique();
			$table->string( 'phone' );
		} );
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */

	// TODO - Make the down()-function for the customersTable
	public function down(){
		Schema::dropIfExists( 'customers' );
	}
}
