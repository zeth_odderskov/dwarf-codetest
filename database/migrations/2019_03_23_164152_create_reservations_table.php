<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'reservations', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->bigInteger('customer_id' )->unsigned();
			$table->foreign('customer_id')->references('id')->on('customers');
			$table->text('drinks_of_choice');
			$table->text('dishes_of_choice');
			$table->dateTime( 'reservation_start' );
			$table->unsignedInteger( 'amount_of_seats' );
		} );
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */

	// Todo - Make the down()-function
	public function down(){
		Schema::dropIfExists( 'reservations' );
	}
}
