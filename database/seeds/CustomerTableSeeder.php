<?php

use App\Customer;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder {

	public function run() {
		$faker = Faker::create();

		foreach( range( 1, 30 ) as $index ){
			Customer::create([
				'name' => $faker->name,
				'email' => $faker->email,
				'phone' => mt_rand( 10000000, 99999999 )
			]);
		} // foreach( range( 1, 10 ) as $index ){
	}
}
?>
