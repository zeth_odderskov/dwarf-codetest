<?php

use App\Reservation;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ReservationTableSeeder extends Seeder {

	public function run() {
		$faker = Faker::create();

		$approved_times = [
			"16:00",
			"18:00",
			"20:00"
		];

		foreach( range( 1, 30 ) as $index ){
			$random_date = $faker->dateTimeBetween( $startDate = '1 week ago', $endDate = '+ 7 days' );
			$modified_random_date = $random_date->format( 'Y-m-d') . ' ' . $approved_times[ mt_rand( 0,2) ];
			$datetime_object = dateTime::createFromFormat( 'Y-m-d H:i', $modified_random_date );

			Reservation::create([
				'customer_id' => mt_rand( 1, 30 ),
				'drinks_of_choice' => $faker->word,
				'dishes_of_choice' => $faker->word,
				'reservation_start' => $datetime_object,
				'amount_of_seats' => mt_rand( 1, 10 )
			]);
		} // foreach( range( 1, 10 ) as $index ){
	}
}
?>
